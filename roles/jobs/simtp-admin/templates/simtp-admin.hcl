job "simtp-admin" {
    datacenters = [ "site1" ]



    group "simtp-admin" {

        network {
            port "http" {
                to = 8080
                static = 8080
            }
        }
        service {
            name = "simtp-admin"
            port = "http"
            check {
                name     = "api-health"
                type     = "http"
                port     = "http"
                path     = "/actuator/health"
                interval = "10s"
                timeout  = "3s"
            }
            tags = ["management.address=http://localhost:8080"]
            meta {
              spring-boot = "true"
            }
        }
        task "simtp-admin" {
            driver = "docker"
            config {
                network_mode = "host"
                image = "simtp/simtp-admin:5-SNAPSHOT"
                force_pull = true

            }
            resources {
                memory = 500
            }

        }

    }
}
