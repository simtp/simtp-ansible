job "simtp-account-api" {
    datacenters = [ "site1" ]


    group "simtp-account-api" {
        count = 3
        spread {
            attribute = "${meta.host}"
            weight = 100
        }
        network {
            port "http" {}
        }
        service {
            name = "simtp-account-api"
            port = "http"
            check {
                name     = "api-health"
                type     = "http"
                port     = "http"
                path     = "/actuator/health"
                interval = "10s"
                timeout  = "3s"
            }
            tags = ["management.address=http://localhost:8080"]
            meta {
              spring-boot = "true"
            }
        }
        task "simtp-account-api" {
            driver = "docker"
            config {
                network_mode = "host"
                image = "simtp/account-api-node:latest"
                force_pull = true
                args = [
                    "--server.port=${NOMAD_PORT_http}",
                ]
            }
            resources {
                memory = 300
            }

        }

    }
}
